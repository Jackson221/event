#include "event.hpp"
//This file is just for unit testing.

#include <cmath>

#include "string_manip/sprintf.hpp"

//used to create a semi-plausible use case for demonstration & unit testing
struct vec3
{
	double x;
	double y;
	double z;
};

bool operator==(vec3 a, vec3 b)
{
	return a.x == b.x && a.y == b.y && a.z == b.z;
}
double distance(vec3 in1, vec3 in2)
{
	return std::sqrt(std::pow(in2.x-in1.x,2)+std::pow(in2.y-in1.y,2)+std::pow(in2.z-in1.z,2));
}

namespace test
{
	constexpr char _internal_vec3_string[] = "{%d%,%d%,%d%}";
	inline std::string convert_to_string(vec3 data) 
	{
		return "";//string_manip::sprintf<_internal_vec3_string>(data.x,data.y,data.z);	
	}
}	
#include "test/test.hpp"

namespace splash_damage
{
	class requirements 
	{
		public:
			requirements(vec3 p, bool i)
			{
				position = p;
				is_using_shield = i;
			}
			requirements(requirements const & other)
			{
				position = other.position;
				is_using_shield = other.is_using_shield;
			}
			requirements& operator=(requirements other)
			{
				using std::swap;
				swap(position, other.position);
				swap(is_using_shield,other.is_using_shield);
				return *this;
			}
			vec3 position;
		   //altenative: vec3& position; //could reference to the position that's stored in the physics object.
			bool is_using_shield;
	};
	struct event_data
	{
		vec3 position;
		int dmg_at_centroid;
		float falloff_rate;
	};
	struct provider
	{
		static bool requirements_check(requirements requirements_structure, event_data event_data)
		{
			return distance(requirements_structure.position, event_data.position) < 2000;
		}
	};

	using event_types_t = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data,requirements, provider>>;
	using manager_t = event_types_t::manager_t;
	using interface_t = event::endpoint_interface<event_types_t>;
	manager_t manager("splash_damage");
	interface_t interface(manager);
}

namespace priority_event
{
	enum class priority
	{
		zero,
		one,
		two,
		SIZE
	};
	struct event_data {};
	//using priority_event_t = event::type<event_data_priority,event::empty_requirements_struct_t,event::empty_requirements_function_provider<event_data_priority>,priority>;
	//priority_event_t priority_event("priority_event");

	template<typename my_configuration_types_t>
	struct event_configuration_types_with_custom_priority
	{
		using manager_t = event::manager<event_configuration_types_with_custom_priority>;
		using configuration_types_t = my_configuration_types_t;
		using interface_types_tuple_t = std::tuple<event::endpoint_interface<event_configuration_types_with_custom_priority,std::mutex,priority>>;
	};

	using event_types_t = event_configuration_types_with_custom_priority<event::event_configuration_types<event_data>>;
	using manager_t = event_types_t::manager_t;
	using interface_t = event::endpoint_interface<event_types_t,std::mutex,priority>;
	manager_t manager("priority_event");
	interface_t myinterface(manager);
}
namespace multiple_interface_event
{
	struct event_data
	{
		int mynum;
	};
	using event_types_t = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data>>;
	using manager_t = event_types_t::manager_t;
	manager_t manager("multiple_interface_event");
	event::endpoint_interface<event_types_t> interf{manager};
	event::endpoint_interface<event_types_t,event::null_mutex> interf_no_mutex{manager};
}

namespace event
{
	void unit_test(test::instance& inst)
	{
		inst.set_name("Event");

		{
			vec3 received_position;
			auto lis = event::listener<splash_damage::interface_t>(splash_damage::interface,[&](splash_damage::event_data data) ->void { received_position=data.position;},  splash_damage::requirements{vec3{3,3,3},false});

			vec3 pushed_position = vec3{1,2,3};
			splash_damage::interface.push_event({pushed_position,2,2.f});
			splash_damage::interface.send_events();
			splash_damage::interface.dispatch_events();

			inst.test(TEST_2(pushed_position,received_position,pushed_position==received_position,true));
		}
		{
			bool got_the_event = false;
			auto lis =  event::listener(splash_damage::interface,[&](splash_damage::event_data data) ->void { got_the_event=true;},  splash_damage::requirements{vec3{9001,3,3},false});

			vec3 pushed_position = vec3{1,2,3};
			splash_damage::interface.push_event({pushed_position,2,2.f});
			splash_damage::interface.send_events();
			splash_damage::interface.dispatch_events();

			inst.inform("Ensure events that don't meet the requirements aren't received");
			inst.test(TEST_0(got_the_event,false));
		}
		{
			bool got_the_event = false;
			{
				auto lis =  event::listener(splash_damage::interface,[&](splash_damage::event_data data) ->void { got_the_event=true;},  splash_damage::requirements{vec3{3,3,3},false});
			}

			vec3 pushed_position = vec3{1,2,3};
			splash_damage::interface.push_event({pushed_position,2,2.f});
			splash_damage::interface.send_events();
			splash_damage::interface.dispatch_events();

			inst.inform("Ensure dead event listeners no longer receive events");
			inst.test(TEST_0(got_the_event,false));
			auto lis =  event::listener(splash_damage::interface,[&](splash_damage::event_data data) ->void { got_the_event=true;},  splash_damage::requirements{vec3{3,3,3},false});
			splash_damage::interface.push_event({pushed_position,2,2.f});
			splash_damage::interface.send_events();
			splash_damage::interface.dispatch_events();

			inst.inform("Ensure cleaned up event listeners do not interfere with new ones");
			inst.test(TEST_0(got_the_event,true));

		}
		{

			bool first_priority_got_the_event = false;
			bool second_priority_saw_first_already_got_the_event = false;
			bool order_was_correct = false;

			//intentionally created out of order; however, priorities should schedule lis0->lis1->lis2

			auto lis1 =  event::listener(priority_event::myinterface,[&](priority_event::event_data data) ->void { second_priority_saw_first_already_got_the_event=first_priority_got_the_event;}, event::empty_requirements_struct_t{}, priority_event::priority::one  );
			auto lis0 =  event::listener(priority_event::myinterface,[&](priority_event::event_data data) ->void { first_priority_got_the_event=true;}, event::empty_requirements_struct_t{}, priority_event::priority::zero  );
			auto lis2 =  event::listener(priority_event::myinterface,[&](priority_event::event_data data) ->void { order_was_correct = second_priority_saw_first_already_got_the_event;}, event::empty_requirements_struct_t{}, priority_event::priority::two  );

			priority_event::myinterface.push_event({});
			priority_event::myinterface.send_events();
			priority_event::myinterface.dispatch_events();

			inst.inform("Ensure event priorities work");
			inst.test(TEST_0(order_was_correct,true));
		}
		{
			int num_listeners_received_event = 0;
			auto my_listener = listener{multiple_interface_event::interf, [&](multiple_interface_event::event_data in){num_listeners_received_event++;},{}};
			auto my_listener_nm = listener{multiple_interface_event::interf_no_mutex, [&](multiple_interface_event::event_data in){num_listeners_received_event++;},{}};

			multiple_interface_event::interf.push_event({27});
			multiple_interface_event::interf.send_events();

			multiple_interface_event::interf.dispatch_events();
			multiple_interface_event::interf_no_mutex.dispatch_events();

			inst.inform("Ensure multiple event interfaces work");
			inst.test(TEST_1(num_listeners_received_event,num_listeners_received_event==2,true));
		}

		//TODO integration test of event requirements + priorities
		//TODO multithreading test (how?)
	}
}
