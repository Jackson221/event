# Event Module

This provides a templated class which is capable of dispatching events to callbacks based on whether they meet the event-receiving requirements, in an order specified by priority. Additionally, it provides a RAII-style listener class that will automatically remove your callback when it falls out of scope. 

# Dependancies

Depends upon module3D's test module

# Use

Simply add the files to your build system, and provide the parent directory of all depandancies as an include flag (i.e. -I"src/" )

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.
