/*!
 * @file
 *
 * @author Jackson McNeill
 *
 * Provides a system by which event pushers and listeners can be created for each event type in a generic, compile-time-generated manner.
 */
#pragma once

#include <vector>
#include <functional>
#include <type_traits>
#include <string>
#include <tuple>
#include <cassert>
#include <utility>
#include <thread>
#include <mutex>

#include "variadic_util/variadic_util.hpp"
#include "test/forward.hpp"

#undef interface

/*!
 * TODO: Ideal pool
 *
 * Threading
 * 	- Could allow "parallelizable groups" similar to priorities
 * 		Priorities would come second to parallelizable. So, it (could) run all
 * 		thread groups simultan. and then within those grouos have priorities for
 * 		one-after-the-other execution
 * 	- Some kind of TCP-esque "I've received your event" system / wait till they've received your event
 * 		Right now the communication is very fire-and-forget.
 * 		The only way to work around this is to make a seperate event that the intendended recipient would fire back on, and wait for that event to trigger.
 * 			-> which means that this system idea could be implemented as a seperate system that uses two events under the hood
 * 		
 *
 * 		Bidirectional requirements check (i.e. add a way for manager or something to do a req check).
 * 			This would be reaallyy, really nice for networking because if the server slaps the request away we could do something.
 * 				Only we'd need to be aware of the requirement failure. (Trigger another event?)
 *
 * 				Basically, deferred requirements check.
 */

namespace event
{
	/*!
	 * Empty structure for use when a event type has no requirements
	 */
	struct empty_requirements_struct_t 
	{
	};

	/*!
	 * Template to provide the requirements check function on an event type which has no requirements.
	 */
	template<typename event_data_t>
	struct empty_requirements_function_provider
	{
		inline static bool requirements_check(empty_requirements_struct_t, event_data_t)
		{
			return true;
		}
	};
	/*!
	 * The default priority levels used.
	 */
	enum class single_priority_level_t
	{
		zeroth,
		SIZE
	};

	class null_mutex
	{	
		public:
			inline null_mutex() {}
			null_mutex(null_mutex const &) = delete;
			null_mutex(null_mutex &&) = delete;
			inline void lock() {}
			inline void unlock() {}
			inline bool try_lock() { return true;}
	};

	/*!
	 * The configuration for a specific event.
	 *
	 * _event_data_t specifies the type that will be passed into callbacks.
	 *
	 * _requirements_structure_t specifies the type that listeners will pass to be used inside of _type_providing_requirement_check
	 * 
	 * _type_providing_requirement_check provides a static function requirements_check with the signature:
	 *
	 *     requirements_check(_requirements_structure_t, _event_data_t)
	 *
	 * _mutex_t is the std::mutex - like class that will be used as a mutex inside of the event manager.
	 */
	template< typename _event_data_t, typename _requirements_structure_t = empty_requirements_struct_t, typename _type_providing_requirement_check = empty_requirements_function_provider<_event_data_t>, typename _mutex_t = std::mutex>
	class event_configuration_types
	{
		public:
			//publicize template args
			using requirements_structure_t = _requirements_structure_t;
			using event_data_t = _event_data_t;
			using type_providing_requirement_check = _type_providing_requirement_check; 

			//This is the type of the callback lambda that event listeners provide.
			using callback_t = std::function<void(event_data_t)>;

			//Avoid vector of bool.
			using queued_events_storage_t = std::conditional_t
			<
				/*if*/ std::is_same_v<bool,event_data_t>, 
				/*then*/ char,
				/*else*/ event_data_t
			>;
			using queued_events_t = std::vector<queued_events_storage_t>;

			using mutex_t = _mutex_t;
	};
	template<typename T>
	using _single_arg_vector_with_pointer_to_type = std::vector<T*>;

	
	template<typename _event_configuration_types_t_t>
	class manager
	{
		public:
			std::string name;

			//get all types from event_configuration_types_t_t::types_t
			using event_configuration_types_t = typename _event_configuration_types_t_t::configuration_types_t;
			using interface_types_tuple = typename _event_configuration_types_t_t::interface_types_tuple_t;
			using requirements_structure_t = typename event_configuration_types_t::requirements_structure_t;
			using event_data_t = typename event_configuration_types_t::event_data_t;
			using queued_events_t = typename event_configuration_types_t::queued_events_t;
			using mutex_t = typename event_configuration_types_t::mutex_t;

			queued_events_t queued_events;

			static constexpr size_t num_interface_types = std::tuple_size_v<interface_types_tuple>;

			variadic_util::make_variadic_of_template_applied_to_variadic_contents_t<_single_arg_vector_with_pointer_to_type,interface_types_tuple> interfaces_container_tuple;
			mutex_t interfaces_container_tuple_mutex;

			/*!
			 * This function is to be called by interfaces when they want to put events into the system.
			 * The order by which events will be sent out to any given interface is undefined. 
			 * On most interfaces, however, local event order is guaranteed (meaning,
			 * if you push event A then B, then dispatch on that same interface, you will get it in the order A then B). 
			 * 
			 * To facilitiate this, interfaces matching the pointer interface_originating_from will
			 * not be dumped the events. Instead, those interfaces should dump the events on themselves,
			 * which will guarantee order by simply being in the same thread.
			 *
			 * It is perfectly legal to set interface_originating_from to nullptr to make all interfaces receive the event.
			 */
			void dump_events(queued_events_t data, void* interface_originating_from)
			{
				auto _ = std::lock_guard(interfaces_container_tuple_mutex);
				variadic_util::for_each_element_in_variadic(interfaces_container_tuple, [&data, &interface_originating_from](auto& interf_container)
				{
					for (auto& interf : interf_container)
					{
						if(reinterpret_cast<void*>(interf) != interface_originating_from)
						{
							interf->dump_events(data);
						}
					}
				});
			}
			template<typename interface_t>
			void push_interface(interface_t* interface)
			{
				auto _ = std::lock_guard(interfaces_container_tuple_mutex);
				variadic_util::for_each_element_in_variadic(interfaces_container_tuple, [&](auto& interf_container)
				{
					using container_type_t = std::remove_pointer_t<typename std::remove_reference_t<decltype(interf_container)>::value_type>;
					if constexpr(std::is_same_v<container_type_t, interface_t>)
					{
						interf_container.push_back(interface);
					}
				});
			}
			//TODO: More effecient storage system so that this doesn't have to look through all interfaces( ineffecient )
			template<typename interface_t>
			void remove_interface(interface_t* interface)
			{
				auto _ = std::lock_guard(interfaces_container_tuple_mutex);
				variadic_util::for_each_element_in_variadic(interfaces_container_tuple, [&](auto& interf_container)
				{
					using container_type_t = std::remove_pointer_t<typename std::remove_reference_t<decltype(interf_container)>::value_type>;
					if constexpr(std::is_same_v<container_type_t, interface_t>)
					{
						auto it = std::find(interf_container.begin(),interf_container.end(), interface);
						interf_container.erase(it);
					}
				});
			}
			
		public:
			manager(std::string name) :
				name(name)
			{
				queued_events.reserve(300);
			}
			//TODO reconsider the name variable as an element of manager altogether. 
			//Note: Its current main use is in identifying events for networking.
			manager() : 
				name("NONAME")
			{
			}
	};


	/*!
	 * This will listen for events given by a specific instantiation of an event type.
	 *
	 * You provide a structure which has data about yourself, which is checked by the event type to see if you
	 * should receive the event at all. If you do receive it, your callback will be called.
	 *
	 * Note: event.hpp's event interfaces use this class, but an event interface does not *have* to use this specific event_listener class.
	 * This is simply the minimum standard one must follow.
	 */
	template<typename interface_t>
	class listener
	{
		using priority_t = typename interface_t::priority_t;
		using requirements_structure_t = typename interface_t::requirements_structure_t;
		using callback_t = typename interface_t::callback_t;
		priority_t priority;
		interface_t* my_interface;
		size_t id;
		bool alive = true;

#ifndef NDEBUG
		std::string manager_name;
		void if_in_debug_mode_then_run_sanity_checks()
		{
			if (id == std::numeric_limits<size_t>::max())
			{
				throw std::runtime_error("Tried to call a method (possibly the destructor) on an event listener after its binded interface died\n\tThis is usually because the event interface got destroyed before a listener. A common case of this is when a global listener is listening to a global interface placed inside another translation unit(.cpp file). The initialization order between translation units is not defined and therefore the listener should either be moved not to be a global or made into a pointer / inside a std::optional");
			}
		}
#else
		inline void if_in_debug_mode_then_run_sanity_checks() {}
#endif
		protected:
			friend interface_t;
			void update_interface_pointer_after_move(interface_t* new_ptr)
			{
				my_interface = new_ptr;
			}

		public:
			listener(interface_t& _my_interface, callback_t callback, requirements_structure_t requirements_structure, priority_t priority = priority_t{0}) :
				priority(priority),
				my_interface(&_my_interface)
			{
				my_interface->new_listener(&id,callback,requirements_structure,priority); //also assigns ID with the pointer
			}
			listener(listener const &) = delete;
			listener& operator=(listener const &) = delete;
			listener(listener && other) : 
				priority(std::move(other.priority)),
				my_interface(std::move(other.my_interface)),
				id(std::move(other.id))
			{
				my_interface->update_id_pointer_after_listener_move(&id, priority);

				other.alive=false;
			}

			listener& operator=(listener && other)
			{
				priority = std::move(other.priority);
				my_interface = std::move(other.my_interface);
				id = std::move(other.id);

				my_interface->update_id_pointer_after_listener_move(&id, priority);

				other.alive=false;

				return *this;
			}

			void update_requirements_structure(requirements_structure_t new_requirements_structure)
			{
				if_in_debug_mode_then_run_sanity_checks();
				my_interface->listener_data[static_cast<size_t>(priority)][id].requirements_structure = new_requirements_structure;
			}
			/*!
			 * Automically called on destructor.
			 *
			 * TODO: Review if this should be removed or not
			 */
			void release()
			{
				if (alive)
				{
					if_in_debug_mode_then_run_sanity_checks();
					my_interface->delete_listener(id,priority);	
					alive = false;
				}
			}
			~listener()
			{
				release();
			}

	};
	template<typename _event_event_configuration_types_t_t, typename _mutex_t = std::mutex>
	struct direct_interface
	{
		using event_types_t_t = _event_event_configuration_types_t_t;
		using event_types_t = typename _event_event_configuration_types_t_t::configuration_types_t;
		using mutex_t = _mutex_t;
		using event_data_t = typename event_types_t::event_data_t;
		using queued_events_t = typename event_types_t::queued_events_t;

		using event_t = typename event_types_t_t::manager_t;

		queued_events_t events_queued_for_listeners;
		mutex_t events_queued_for_listners_mutex;
		void dump_events(queued_events_t in)
		{
			auto _ = std::lock_guard(events_queued_for_listners_mutex);
			events_queued_for_listeners.insert(events_queued_for_listeners.end(),in.begin(),in.end());
#ifdef DEBUG
			if (events_queued_for_listeners.size() > 300)
			{
				printf("Warning - An event endpoint interface has more than 300 events waiting for dispatch_events() to be called on\n");
			}
#endif
		}
		inline queued_events_t fetch_events()
		{
			auto _ = std::lock_guard(events_queued_for_listners_mutex);

			queued_events_t result = std::move(events_queued_for_listeners);

			events_queued_for_listeners = {};

			return result;
		}

		event_t* pointer_to_host_event;
		direct_interface(event_t& event)
		{
			event.push_interface(this);
			pointer_to_host_event =(&event);
			events_queued_for_listeners.reserve(300);
		}
		direct_interface& operator=(direct_interface && other)
		{
			pointer_to_host_event = other.pointer_to_host_event;
			pointer_to_host_event->remove_interface(&other);
			other.pointer_to_host_event = nullptr;
			pointer_to_host_event->push_interface(this);
		}
		direct_interface(direct_interface && other)
		{
			*this = other;
		}
		direct_interface& operator=(direct_interface const & other)
		{
			pointer_to_host_event = other.pointer_to_host_event;
			pointer_to_host_event->push_interface(this);
			return *this;
		}
		direct_interface(direct_interface const & other)
		{
			*this = other;
		}
		~direct_interface()
		{
			if (pointer_to_host_event)
			{
				pointer_to_host_event->remove_interface(this);
			}
		}
	};
	/*!
	 * _event_event_configuration_types_t_t is a class providing aliases for various types needed by the endpoint interface. Most of the time, you will just use event_configuration_types_with_default_interfaces
	 * However, if you wish to allow additional endpoint interfaces to be used in addition / rather than the default endpoint_interface, you can define a type which provides all the type aliases that event_configuration_types_with_default_interfaces
	 * defines.
	 *
	 * _mutex_t is the std::mutex - like class that will be used as a mutex inside of the event manager.
	 *
	 * _priority_t is the type (probably an enum) you provide for priorities. It must be convertible to size_t, it must have a list constructor allowing a single int with the value 0,
	 * it must provide a member "SIZE" which is convertible to size_t, and priorities must not be given bigger than SIZE. No diagnostic must be given
	 * if you provide a priority larger than SIZE. Listeners with lower priority values run before others with a higher priority value. The order in which 
	 * events are run in one thread relative to another thread is not defined by priority.
	 *
	 * The recommended implementation for _priority_t is an enum class with "SIZE" at the end of it. Due to prior constaints that all priorities
	 * must be convertible to size_t, negative values cannot be used.
	 */
	template<typename _event_event_configuration_types_t_t, typename _mutex_t = std::mutex, typename _priority_t = single_priority_level_t>
	class endpoint_interface
	{
		public:
			using event_types_t_t = _event_event_configuration_types_t_t;
			using event_types_t = typename _event_event_configuration_types_t_t::configuration_types_t;
			using mutex_t = _mutex_t;
			using priority_t = _priority_t;
			using type_providing_requirement_check = typename event_types_t::type_providing_requirement_check;
			using callback_t = typename event_types_t::callback_t;
			using requirements_structure_t = typename event_types_t::requirements_structure_t;
			using event_data_t = typename event_types_t::event_data_t;
			using queued_events_t = typename event_types_t::queued_events_t;

			using event_t = typename event_types_t_t::manager_t;

			queued_events_t events_queued_for_listeners;
			mutex_t events_queued_for_listners_mutex;

			queued_events_t events_queued_for_event;

			//event_t may be an incomplete type during some instantiations of this template (like the one that event_t itself uses)
			//However, it is okay for have a pointer to an incomplete type.
			event_t* pointer_to_host_event;

			std::string ifname = "unnamed";

			void dump_events(queued_events_t in)
			{
				auto _ = std::lock_guard(events_queued_for_listners_mutex);
				events_queued_for_listeners.insert(events_queued_for_listeners.end(),in.begin(),in.end());
#ifdef DEBUG
				if (events_queued_for_listeners.size() > 300)
				{
					printf("Warning - An event endpoint interface has more than 300 (%lu) events waiting for dispatch_events() to be called on\n\tEvent interface %s dispatches type %s\n",
							events_queued_for_listeners.size(), ifname.c_str(), typeid(event_data_t).name());
				}
#endif

			}

			/*!
			 * Sends events to listeners first-in-first-out
			 *
			 * Must not be called simultaniously in different threads
			 *
			 * Note: It is undefined behavior to call this function during an event callback of the same event instance.
			 */
			void dispatch_events()
			{
				queued_events_t old_queued_events;
				{
					auto _ = std::lock_guard(events_queued_for_listners_mutex);
					old_queued_events = std::move(events_queued_for_listeners);
					events_queued_for_listeners = {};
				}
				for (auto& event_data : old_queued_events)
				{
					size_t priority_it = 0;
					std::vector<callback_t> to_be_called; //TODO: better to do this or store an iterator? ; Provide some sort of method in template args for user to guess how many will be called per dispatch_events and then call reserve(#)
					
					for (auto& this_priority_listener_data : listener_data)
					{
						size_t listener_it = 0;
						for (auto& this_listener_data : this_priority_listener_data)
						{
							if ( does_listener_exist[priority_it][listener_it])
							{
								if (type_providing_requirement_check::requirements_check(this_listener_data.requirements_structure,event_data))
								{
									//this_listener_data.callback(event_data);
									to_be_called.push_back(this_listener_data.callback);
								}
							}
							listener_it++;
						}
						priority_it++;
					}
					for (auto& fn : to_be_called)
					{
						fn(event_data);
					}
				}
			}

			void push_event(event_data_t data)
			{
				events_queued_for_event.push_back(data);
#ifdef DEBUG
				if (events_queued_for_event.size() > 300)
				{
					printf("Warning - An event endpoint interface has more than 300 events which are waiting send_events() to be called on\n");
				}
#endif

			}

			//Send all events pushed by push_event to the main event class
			//Preserves the order of events *for listeners registered to this interface only!*
			void send_events()
			{
				//This is okay because event::manager will not instantiate member functions it does not use (and it will never use this one).
				//So, it is NOT recursive templating to do this.
				pointer_to_host_event->dump_events(events_queued_for_event,this);
				//Give events to our listeners -- the event won't do this for us (so that we can ensure order)
				if (events_queued_for_listeners.empty())
				{
					//If there are no events queued, then it is more effecient to swap the two queues.
					std::swap(events_queued_for_event,events_queued_for_listeners);
				}
				else
				{
					events_queued_for_listeners.insert(events_queued_for_listeners.end(), events_queued_for_event.begin(), events_queued_for_event.end());	
					events_queued_for_event = {};
				}
			}

		protected:
			friend listener<endpoint_interface>;
			
			/*!
			 * Creates a new listener and adds it to my records.
			 *
			 * This may allocate more space on the listener_data vector, or it will overwrite no-longer-used spots.
			 */
			void new_listener(size_t* id, callback_t callback, requirements_structure_t requirements_structure, priority_t priority)
			{
				size_t it = static_cast<size_t>(priority);
				
				auto& this_priority_listener_vector = listener_data[it];	
				auto& this_priority_exists_vector = does_listener_exist[it];
				auto& this_priority_spot_vector = available_spots[it];

				if (this_priority_spot_vector.empty())
				{
					size_t new_id = this_priority_listener_vector.size();

					this_priority_listener_vector.emplace_back(id,callback,requirements_structure);	
					this_priority_exists_vector.emplace_back(true);
					assert(this_priority_listener_vector.size() == this_priority_exists_vector.size());

					*id = new_id;
				}
				else
				{
					size_t new_id = this_priority_spot_vector.back();

					this_priority_spot_vector.pop_back();
					this_priority_listener_vector[new_id] = listener_data_t{id,callback,requirements_structure};
					this_priority_exists_vector[new_id] = true;

					*id = new_id;
				}
			}
			/*!
			 * Must call after the id pointer points to your actual ID number
			 *
			 * i.e. populate the data pointed to by size_t* id first.
			 */
			void update_id_pointer_after_listener_move(size_t* id, priority_t priority)
			{
				//find the listener in our records, then update the ID pointer to the new one
				listener_data[static_cast<size_t>(priority)][*id].id = id;
			}
			/*!
			 * Deletes an existing listener from my records. Throws exceptions if it doesn't exist.
			 */
			void delete_listener(size_t id, priority_t priority)
			{
				size_t it = static_cast<size_t>(priority);
				auto& this_priority_exists_vector = does_listener_exist[it];
				auto& this_priority_spot_vector = available_spots[it];

				if (this_priority_exists_vector[id])
				{
					this_priority_spot_vector.push_back(id);
					this_priority_exists_vector[id] = false;
				}
				else
				{
					throw std::runtime_error("attempt to double-delete event listener");
				}
			}
			/*!
			 * Provides the event manager data about its listeners.
			 */
			struct listener_data_t
			{
				listener_data_t(size_t* _id, callback_t _callback, requirements_structure_t _requirements_structure) :
					id(_id),
					callback(_callback),
					requirements_structure(_requirements_structure)
				{
				}
				listener_data_t()
				{
				}
				size_t* id;
				callback_t callback;
				requirements_structure_t requirements_structure;
			};

			//TODO: perf test does_listener_exist vs. rearranging vector
			// -- or rather, keeping an additional structure which rearranges itself (since ID's must be kept constant)

			/*!
			 * Contains data about a specific listener. Every existing event_listener has a structure in here
			 * 
			 * Before using anything inside here, you must check if it's still alive with does_listener_exist.
			 */
			std::array<std::vector<listener_data_t>,static_cast<size_t>(priority_t::SIZE)> listener_data = {};
			/*!
			 * For every entry in listner_data there exists a bool in this that is true if the listener who spawned it still exists.
			 */
			std::array<std::vector<char>,static_cast<size_t>(priority_t::SIZE)> does_listener_exist = {};
			/*!
			 * Contains all IDs in which does_listener_exist[ID] == false.
			 */
			std::array<std::vector<size_t>,static_cast<size_t>(priority_t::SIZE)> available_spots = {};

		public:

			endpoint_interface(event_t& event)
			{
				event.push_interface(this);

				pointer_to_host_event =(&event);

				events_queued_for_listeners.reserve(300);
				events_queued_for_event.reserve(300);
			}
			endpoint_interface(event_t& event, std::string in_ifname) :
				endpoint_interface(event)
			{
				ifname = in_ifname;
			}
			endpoint_interface(endpoint_interface const &) = delete;
			endpoint_interface(endpoint_interface &&) = delete;
			endpoint_interface& operator=(endpoint_interface const &) = delete;
			endpoint_interface& operator=(endpoint_interface &&) = delete;
			~endpoint_interface()
			{
#ifndef NDEBUG
				//Tell our interfaces we're dead so they can throw exceptions if the interfaces are used after our death
				//Only in debug mode because this is not necessary for properly behaving code
				size_t priority_it = 0;
				for (auto& this_priority_listener_data : listener_data)
				{
					size_t listener_it = 0;
					for (auto& this_listener_data : this_priority_listener_data)
					{
						if ( does_listener_exist[priority_it][listener_it])
						{
							*this_listener_data.id = std::numeric_limits<size_t>::max();
						}
						listener_it++;
					}
					priority_it++;
				}
#endif

				pointer_to_host_event->remove_interface(this);
			}
	};

	/*!
	 * If you only wish to use the default endpoint interface ( event::endpoint_interface ) then you can use this to instatiate your event type
	 * Just pass in your configuration, and this class will alias manager_t to the event manager type, 
	 * and then you can pass this class into your endpoint_interface template parameters to instantiate them.
	 */
	template<typename my_configuration_types_t>
	struct event_configuration_types_with_default_interfaces
	{
		using manager_t = manager<event_configuration_types_with_default_interfaces>;
		using configuration_types_t = my_configuration_types_t;
		using interface_types_tuple_t = std::tuple
		<
			endpoint_interface<event_configuration_types_with_default_interfaces>,
			endpoint_interface<event_configuration_types_with_default_interfaces, null_mutex>,
			direct_interface<event_configuration_types_with_default_interfaces>
		>;
	};
	template<typename event_data_t>
	struct regular_event_types 
	{
		using event_configuration_types = event::event_configuration_types_with_default_interfaces<event::event_configuration_types<event_data_t>>;
		using manager_t = event_configuration_types::manager_t;
		using interface_t = event::endpoint_interface<event_configuration_types>;
	};

	void unit_test(test::instance& inst);
}
